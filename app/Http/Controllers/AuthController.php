<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use stdClass;

class AuthController extends Controller
{

    //Crear Usuario
    public function register(Request $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);

        if (!Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            return response()->json([
                'message' => 'Invalid Credentials'
            ], 401);
        }

        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'token' => $token,
            'token_type' => 'Bearer',
            'user' => $user,
        ], 201);
    }

    public function login(Request $request)
    {
        //Identificar tipo de login
        $login = $request['login'];
        $password = $request['password'];

        //Verificar si logeo con email o con username
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        //Si falla retornar error
        if (!Auth::attempt([$field => $login, 'password' => $password])) {
            return response()->json([
                'message' => 'Invalid Credentials'
            ], 401);
        }

        if ($field == 'email') {
            $user = User::where('email', $request['login'])->firstOrFail();
        } else {
            $user = User::where('username', $request['login'])->firstOrFail();
        }
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'token' => $token,
            'token_type' => 'Bearer',
            'user' => $user,
        ], 200);
    }
}
