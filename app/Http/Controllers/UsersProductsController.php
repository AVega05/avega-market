<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Users_Products;
use Illuminate\Http\Request;

class UsersProductsController extends Controller
{
    public function purchase(Request $request)
    {
        $user = $request->user();
        $user_id = $user['id'];
        $purchase = Users_Products::create([
            'price' => $request['price'],
            'user_id' => $user_id,
            'product_id' => $request['product_id']
        ]);

        $product = Product::where('id', $request['product_id'])->first();
        $product = $product->update(['stock' => $product->stock - 1]);

        return response($purchase, 201);
    }

    public function myPurchases(Request $request)
    {
        $user = $request->user();
        $id = $user['id'];
        $purchases = Users_Products::with('user', 'product')
            ->join('products', 'users_products.product_id', '=', 'products.id')
            ->join('users', 'products.user_id', '=', 'users.id')
            ->select('users_products.*', 'products.user_id as user_product_id', 'users.name as seller')
            ->where('users_products.user_id', '=', $id)
            ->orWhere('products.user_id', '=', $id)
            ->orderBy('created_at', 'desc')
            ->get();
        return response($purchases, 200);
    }
}
