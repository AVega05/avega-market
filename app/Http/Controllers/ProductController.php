<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function store(Request $request)
    {
        $user = $request->user();
        $user_id = $user['id'];
        $product = Product::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'price' => $request['price'],
            'stock' => $request['stock'],
            'state' => 1,
            'user_id' => $user_id,
            'category_id' => $request['category_id'],
        ]);
        return response($product, 201);
    }

    public function desactivateProduct(Request $request)
    {

        $product = Product::where('id', $request['product_id'])->first();
        $product = $product->update(['state' => 0]);

        return response($product, 201);
    }

    public function getMyProducts(Request $request)
    {
        $user = $request->user();
        if (is_null($user)) {
            return response('Unauthorized',401);
        }
        $id = $user['id'];
        $products = Product::with("category")->where('user_id', $id)->where('state', 1)->orderBy('created_at', 'desc')->get();
        return response($products, 200);
    }
    public function getMarketProducts(Request $request)
    {
        $user = $request->user();
        $id = $user['id'];
        $products = Product::with("user", "category")->where('user_id', '!=', $id)->where('state', 1)->orderBy('created_at', 'desc')->get();
        return response($products, 200);
    }
}
