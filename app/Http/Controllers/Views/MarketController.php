<?php

namespace App\Http\Controllers\Views;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;


class MarketController extends Controller
{
    public function homeView(Request $request)
    {

        $user = $request->user();
        $username = $user['username'];

        $token = Cookie::get('token');
        if (is_null($token)) {
            return redirect()->route('login.index');
        }
        $request = Request::create('/api/products/market', 'GET');
        $request->headers->set('Autorization', 'Bearer', $token);
        $response = Route::dispatch($request);
        if ($response->status() != 200) {
            return redirect()->route('login.index');
        }

        $marketProducts = $response->original;
        return view('pages/home', ['marketProducts' => $marketProducts, 'username' => $username]);
    }

    public function myProductsView(Request $request)
    {
        $user = $request->user();
        $username = $user['username'];

        $token = Cookie::get('token');
        if (is_null($token)) {
            return redirect()->route('login.index');
        }

        $request = Request::create('/api/products/my', 'GET');
        $request->headers->set('Autorization', 'Bearer', $token);
        $response = Route::dispatch($request);
        if ($response->status() != 200) {
            return redirect()->route('login.index');
        }
        $myProducts = $response->original;


        $requestCat = Request::create('/api/category', 'GET');
        $requestCat->headers->set('Autorization', 'Bearer', $token);
        $responseCat = Route::dispatch($requestCat);
        $categories = $responseCat->original;
        return view('pages/myproducts', ['myProducts' => $myProducts, 'categories' => $categories, 'username' => $username]);
    }

    public function myPurchasesView(Request $request)
    {
        $user = $request->user();

        $token = Cookie::get('token');
        if (is_null($token)) {
            return redirect()->route('login.index');
        }

        $request = Request::create('/api/my-purchases', 'GET');
        $request->headers->set('Autorization', 'Bearer', $token);
        $response = Route::dispatch($request);

        if ($response->status() != 200) {
            return redirect()->route('login.index');
        }

        $Purchases = $response->original;

        $myPurchases = array();
        $mySales = array();
        foreach ($Purchases as $key => $purchase) {
            if ($purchase->user_product_id == $user->id) {
                array_push($mySales, $purchase);
            }
            if ($purchase->user_id == $user->id) {
                array_push($myPurchases, $purchase);
            }
        }


        return view('pages/mypurchases', ['myPurchases' => $myPurchases, 'mySales' => $mySales, 'user' => $user]);
    }
}
