<?php

namespace App\Http\Controllers\Views;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

class BusinessLogicController extends Controller
{
    public function makePurchase(Request $request)
    {
        $token = Cookie::get('token');
        $request = Request::create('/api/purchase', 'POST');
        $request->headers->set('Autorization', 'Bearer', $token);
        Route::dispatch($request);

        return redirect()->route('home');
    }
    public function addProduct(Request $request)
    {
        $token = Cookie::get('token');
        $request = Request::create('/api/product', 'POST');
        $request->headers->set('Autorization', 'Bearer', $token);
        Route::dispatch($request);

        return redirect()->route('myproducts');
    }

    public function desactivateProduct(Request $request)
    {
        $token = Cookie::get('token');
        $request = Request::create('/api/product/desactivate', 'POST');
        $request->headers->set('Autorization', 'Bearer', $token);
        Route::dispatch($request);

        return redirect()->route('myproducts');
    }
}
