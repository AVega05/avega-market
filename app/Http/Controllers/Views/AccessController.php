<?php

namespace App\Http\Controllers\Views;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

class AccessController extends Controller
{

    public function initLoginView()
    {
        return view('pages/login');
    }
    public function initRegisterView()
    {
        return view('pages/register');
    }
    public function register(Request $request)
    {

        $request = Request::create('/api/register', 'POST');

        $response = Route::dispatch($request);
        $token = $response->original['token'];
        Cookie::queue('token', $token);

        return redirect()->route('home');
    }
    public function login(Request $request)
    {

        $request = Request::create('/api/login', 'POST');

        $response = Route::dispatch($request);
        $statusCode = $response->status();
        if ($statusCode == 401) {
            return redirect()->route('login.index')->with('message','Invalid Credentials');
        }
        $token = $response->original['token'];
        Cookie::queue('token', $token);

        return redirect()->route('home');
    }
}
