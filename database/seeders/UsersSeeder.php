<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Alvaro Vega',
            'username' => 'Alvarrox',
            'email' => 'alvarrox@gmail.com',
            'password' => Hash::make('123456'),
        ]);
        DB::table('users')->insert([
            'name' => 'Benjamin Vega',
            'username' => 'Benjarrox',
            'email' => 'benjarrox@gmail.com',
            'password' => Hash::make('654321'),
        ]);

    }
}
