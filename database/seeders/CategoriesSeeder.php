<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Hogar',
        ]);
        DB::table('categories')->insert([
            'name' => 'Tecnologia',
        ]);
        DB::table('categories')->insert([
            'name' => 'Belleza',
        ]);
        DB::table('categories')->insert([
            'name' => 'Vestuario',
        ]);
        DB::table('categories')->insert([
            'name' => 'Salud',
        ]);
        DB::table('categories')->insert([
            'name' => 'Mascotas',
        ]);
    }
}
