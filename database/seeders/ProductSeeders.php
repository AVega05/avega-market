<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Software',
            'description' => 'Laravel Challenge',
            'price' => '5.5',
            'stock' => '5',
            'user_id' => '1',
            'category_id' => '2',
            'state' => '1'
        ]);
        DB::table('products')->insert([
            'name' => 'MiBand 6',
            'description' => 'Smart Band',
            'price' => '40',
            'stock' => '4',
            'user_id' => '1',
            'category_id' => '2',
            'state' => '1'

        ]);
        DB::table('products')->insert([
            'name' => 'Eloboost',
            'description' => 'win rankeds',
            'price' => '12.5',
            'stock' => '10',
            'user_id' => '2',
            'category_id' => '2',
            'state' => '1'

        ]);
        DB::table('products')->insert([
            'name' => 'Doctor en casa',
            'description' => 'Servicio de medicina a domicilio',
            'price' => '60',
            'stock' => '2',
            'user_id' => '2',
            'category_id' => '5',
            'state' => '1'

        ]);
    }
}
