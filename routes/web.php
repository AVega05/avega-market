<?php

use App\Http\Controllers\Views\BusinessLogicController;
use App\Http\Controllers\Views\AccessController;
use App\Http\Controllers\Views\MarketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/login');
});
//Register View
Route::get('/register', [AccessController::class, 'initRegisterView'])->name('register.index');
Route::post('/register', [AccessController::class, 'register'])->name('register.access');

//Login View
Route::get('/login', [AccessController::class, 'initLoginView'])->name('login.index');
Route::post('/login', [AccessController::class, 'login'])->name('login.access');

//Market Views
Route::get('/home', [MarketController::class, 'homeView'])->name('home');
Route::get('/my-products', [MarketController::class, 'myProductsView'])->name('myproducts');
Route::get('/my-purchases', [MarketController::class, 'myPurchasesView'])->name('mypurchases');

//Business Logic 
Route::post('/purchase', [BusinessLogicController::class, 'makePurchase'])->name('make.purchase');
Route::post('/product', [BusinessLogicController::class, 'addProduct'])->name('make.product');
Route::post('/product-desactivate', [BusinessLogicController::class, 'desactivateProduct'])->name('desactivate.product');
