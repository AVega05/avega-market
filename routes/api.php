<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UsersProductsController;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {

    //Lista de categorias
    Route::get('/category', [CategoryController::class, 'index']);

    //Administrar productos
    Route::post('/product', [ProductController::class, 'store']);
    Route::post('/product/desactivate', [ProductController::class, 'desactivateProduct']);
    Route::get('/products/my', [ProductController::class, 'getMyProducts']);
    Route::get('/products/market', [ProductController::class, 'getMarketProducts']);

    //Administrar compras
    Route::post('/purchase', [UsersProductsController::class, 'purchase']);
    Route::get('/my-purchases', [UsersProductsController::class, 'myPurchases']);
    
});
