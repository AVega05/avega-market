<nav class="bg-gray-900 shadow dark:bg-gray-800 mb-5">
    <div class="container px-6 py-3 mx-auto md:flex md:justify-between md:items-center">
        <div class="flex items-center justify-between">
            <div>
                <p class="text-xl font-bold text-gray-50 dark:text-white md:text-2xl">
                    @yield('username')
                    Market</p>
            </div>
        </div>

        <!-- Mobile Menu open: "block", Menu closed: "hidden" -->
        <div class="items-center md:flex">
            <div class="flex flex-col md:flex-row md:mx-6">
                <a class="my-1 text-gray-50 dark:text-gray-200 hover:text-green-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
                    href="{{ url('/home') }}">Market</a>
                <a class="my-1 text-gray-50 dark:text-gray-200 hover:text-green-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
                    href="{{ url('/my-products') }}">Products</a>
                <a class="my-1 text-gray-50 dark:text-gray-200 hover:text-green-500 dark:hover:text-indigo-400 md:mx-4 md:my-0"
                    href="{{ url('/my-purchases') }}">Transactions</a>
            </div>
        </div>
    </div>
</nav>
