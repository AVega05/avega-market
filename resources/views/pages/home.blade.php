@extends('layouts.default')
@section('username')
    {{ $username }}
@endsection
@section('content')


    <div class="grid grid-cols-2 gap-4">

        <div class="inline">
            <h2 class="my-5 text-3xl lg:text-4xl text-gray-800 font-light mb-10">
                Your Market
            </h2>
        </div>

    </div>

    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg mb-5">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Seller
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Name
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Publication Date
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Description
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Category
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Price
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Stock
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        @if (count($marketProducts) == 0)
                            <tbody class="bg-white divide-y divide-gray-200">
                                <tr>
                                    <td colspan="4">
                                        <div class="inline">
                                            <p class="mx-5 my-5 text-base text-gray-800">
                                                No products to buy
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        @else
                            @foreach ($marketProducts as $product)
                                @if ($product->stock > 0)
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        <tr>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $product->user->name }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $product->name }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $product->created_at }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $product->description }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $product->category->name }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-center">
                                                <div class="text-sm text-gray-900">{{ $product->price }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-center">
                                                <div class="text-sm text-gray-900">{{ $product->stock }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap text-center">
                                                <form method="post" action="{{ route('make.purchase') }}">
                                                    @csrf
                                                    <input type="number" class="form-control hidden" name="price"
                                                        value='{{ $product->price }}'>
                                                    <input type="number" class="form-control hidden" name="product_id"
                                                        value='{{ $product->id }}'>
                                                    <button type="submit"
                                                        class="px-4 py-2 w-full inline-flex justify-center bg-transparent border border-green-500 text-green-500 text-lg rounded-lg hover:bg-green-500 hover:text-gray-100 focus:border-4 focus:border-green-300">
                                                        Buy
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                @endif
                            @endforeach
                        @endif

                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
