@extends('layouts.default')
@section('username')
    {{ $username }}
@endsection
@section('content')

    <div class="grid grid-cols-2 gap-4">
        <div class="inline">
            <h2 class="my-5 text-3xl lg:text-4xl text-gray-800 font-light mb-10">
                Your Products
            </h2>
        </div>
        <div class="inline text-right">
            <button type="button"
                class="mx-5 my-5 p-2 pl-5 pr-5 bg-transparent border-2 border-green-500 text-green-500 text-lg rounded-lg hover:bg-green-500 hover:text-gray-100 focus:border-4 focus:border-green-300 openModal">
                Add Product</button>
        </div>
    </div>

    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg mb-5">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50 ">
                            <tr>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Name
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Description
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Category
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Price
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Stock
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        @if (count($myProducts) == 0)
                            <tbody class="bg-white divide-y divide-gray-200">
                                <tr>
                                    <td colspan="4">
                                        <div class="inline">
                                            <p class="mx-5 my-5 text-base text-gray-800">
                                                You currently have no products
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        @else
                            @foreach ($myProducts as $product)
                                <tbody class="bg-white divide-y divide-gray-200">
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $product->name }}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $product->description }}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $product->category->name }}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-center">
                                            <div class="text-sm text-gray-900">{{ $product->price }}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-center">
                                            <div class="text-sm text-gray-900">{{ $product->stock }}</div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap text-center">
                                            <form action="{{ route('desactivate.product') }}" method="post">
                                                @csrf
                                                <input type="number" class="form-control hidden" name="product_id"
                                                    value='{{ $product->id }}'>
                                                <button type="submit"
                                                    class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-500 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>



    <div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true"
        id="interestModal">

        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>

            <div
                class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle ">

                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">

                    <div class="sm:flex sm:items-start">


                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">

                            <h3 class="text-lg text-center leading-6 font-medium text-gray-50 bg-gray-900" style="width: 500px"
                                id="modal-title">

                                Add Product

                            </h3>

                            <div class="mt-2">
                                <form action="{{ route('make.product') }}" method="post">
                                    @csrf
                                    <div class="flex flex-col mb-4">
                                        <label class="mb-2 font-bold text-lg text-gray-900" for="name">Name</label>
                                        <input class="border py-2 px-3 text-grey-800" type="text" name="name" id="name"
                                            required>
                                    </div>
                                    <div class="flex flex-col mb-4">
                                        <label class="mb-2 font-bold text-lg text-gray-900"
                                            for="description">Description</label>
                                        <textarea class="border py-2 px-3 text-grey-800" type="textarea" name="description"
                                            id="description" maxlength="255" required> </textarea>
                                    </div>
                                    <div class="flex flex-col mb-4">
                                        <label class="mb-2 font-bold text-lg text-gray-900"
                                            for="description">Category</label>
                                        <select class="border py-2 px-3 text-grey-800" type="text" name="category_id"
                                            id="category">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="flex flex-col mb-4">
                                        <label class="mb-2 font-bold text-lg text-gray-900" for="price">Price</label>
                                        <input class="border py-2 px-3 text-grey-800" type="number" min="0.1" step="0.1"
                                            name="price" id="price" required>
                                    </div>
                                    <div class="flex flex-col mb-4">
                                        <label class="mb-2 font-bold text-lg text-gray-900" for="stock">Stock</label>
                                        <input class="border py-2 px-3 text-grey-800" type="number" min="1" name="stock"
                                            id="stock" required>
                                    </div>
                                    <div class="flex flex-col mb-4 mx-5">
                                        <button type="submit"
                                            class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-green-500 text-base font-medium text-white hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">

                                            Add

                                        </button>
                                    </div>
                                    <div class="flex flex-col mb-4 mx-5">

                                        <button type="button"
                                            class="closeModal mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">

                                            Cancel

                                        </button>
                                    </div>

                                </form>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>


    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            $('.openModal').on('click', function(e) {

                $('#interestModal').removeClass('invisible');

            });

            $('.closeModal').on('click', function(e) {

                $('#interestModal').addClass('invisible');

            });

        });
    </script>


@stop
