@extends('layouts.default')
@section('username')
    {{ $user->username }}
@endsection
@section('content')

    {{-- Sales Table --}}
    <div class="grid grid-cols-2 gap-4">

        <div class="inline">
            <h2 class="my-5 text-3xl lg:text-4xl text-gray-800 font-light mb-10">
                Your Sales
            </h2>
        </div>
    </div>
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg mb-5">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Product
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Buyer
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Price
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    date of purchase
                                </th>
                            </tr>
                        </thead>
                        @if (count($mySales) == 0)
                            <tbody class="bg-white divide-y divide-gray-200">
                                <tr>
                                    <td colspan="4">
                                        <div class="inline">
                                            <p class="mx-5 my-5 text-base text-gray-800">
                                                You currently have no sales
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        @else
                            @foreach ($mySales as $sale)
                                @if ($sale->user_product_id == $user->id)
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        <tr>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $sale->product->name }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $sale->user->name }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $sale->price }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $sale->created_at }}</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                @endif

                            @endforeach
                        @endif

                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Purchases Table --}}
    <div class="grid grid-cols-2 gap-4">

        <div class="inline">
            <h2 class="my-5 text-3xl lg:text-4xl text-gray-800 font-light mb-10">
                Your Purchases
            </h2>
        </div>

    </div>
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg mb-5">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Product
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Seller
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Price
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    date of purchase
                                </th>
                            </tr>
                        </thead>
                        @if (count($myPurchases) == 0)
                            <tbody class="bg-white divide-y divide-gray-200">
                                <tr>
                                    <td colspan="4">
                                        <div class="inline">
                                            <p class="mx-5 my-5 text-base text-gray-800">
                                                You currently have no purchases
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        @else
                            @foreach ($myPurchases as $purchase)
                                @if ($purchase->user_id == $user->id)
                                    <tbody class="bg-white divide-y divide-gray-200">
                                        <tr>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $purchase->product->name }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $purchase->seller }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $purchase->price }}</div>
                                            </td>
                                            <td class="px-6 py-4 whitespace-nowrap">
                                                <div class="text-sm text-gray-900">{{ $purchase->created_at }}</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                @endif

                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
