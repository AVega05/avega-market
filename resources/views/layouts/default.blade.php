<!DOCTYPE html>
<html lang="es">

<head>
    @include('includes.head')
</head>

<body>
    <div>
        <header>
            @include('includes.header')
        </header>

        <div id="main" class="container mx-auto">

            @yield('content')

        </div>

        <div class="container mx-auto">
            <footer class="row">
                @include('includes.footer')
            </footer>
        </div>
    </div>


</body>

</html>
